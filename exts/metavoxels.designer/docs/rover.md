# Rover

![](../data/videos/rover_design.mp4)

## Graph


```mermaid
graph TB

    Vc-.->V1
    Vc-.->Ø
    Vc-.->Ø
    Vc-.->Ø
    Vc-.->Ø
    Vc-.->Ø


    V1-.->Ø1[Ø]
    V1-.->Ø1[Ø]
    V1-.->E1
    V1-.->E
    V1-.->E

    E1-.->V2

    V2-.->Ø2[Ø]
    V2-.->Ø2[Ø]
    V2-.->Ø2[Ø]
    V2-.->E
    V2-.->E

    E-.->W
    W-.->V

    V-.->Ø3[Ø]
    V-.->Ø3[Ø]
    V-.->Ø3[Ø]
    V-.->Ø3[Ø]
    V-.->Ø3[Ø]




```
