
# MetaVoxels Discrete Robotic Toolkit

## Background 

<img src="../data/videos/3_bille_real.gif" width="30%"/>

The discrete robotic toolkit was introduced in:

Abdel-Rahman, Amira, Christopher Cameron, Benjamin Jenett, Miana Smith and Neil Gershenfeld. "Self-Replicating Hierarchical Modular Robotic Swarms". Nature Communications Engineering. Accepted.

and

Cameron, Christopher G., Zach Fredin, and Neil Gershenfeld. "Discrete Assembly of Unmanned Aerial Systems." 2022 International Conference on Unmanned Aircraft Systems (ICUAS). IEEE, 2022. [Link](https://cba.mit.edu/docs/papers/22.06.ICUAS.pdf).


<img src="../data/videos/1_tendon_real.gif" width="30%"/>

The compliant members are based on the rigid and compliant faces introduced in:

Jenett, Benjamin, et al. "Discretely assembled mechanical metamaterials." Science advances 6.47 (2020): eabc9943. [Link](https://cba.mit.edu/docs/papers/20.11.meta_combined.pdf).

---

# MetaVoxels Recursive Grammar Rules

Grammar formulation based on [1](https://people.csail.mit.edu/jiex/papers/robogrammar/index.html).
```math
\mathcal{G}=(N,T,A,R,\mathcal{S})
```

- $`N`$: non terminal symbols
- $`T`$: terminal symbols
- $`A`$: attributes
- $`R`$: rules
- $`S`$: start symbol

## Non Terminal Symbols - $`N`$
- $`M`$: Placeholder for voxel or joint
- $`M_c`$: Placeholder for voxel or joint or compliant voxel
- $`J`$: Placeholder for joint (elbow to wrist)

## Terminal Symbols - $`T`$
- $`V_c`$: controller voxel

<img src="../data/vox_lib/Vc.png" width="10%"/>

- $`V`$: rigid voxel

<img src="../data/vox_lib/V.png" width="10%"/>

- $`Je`$: elbow joint

<img src="../data/vox_lib/Je.png" width="10%"/>

- $`Jw`$: wrist joint

<img src="../data/vox_lib/Jw.png" width="10%"/>

- $`Va`$: tendon actuator voxel

<img src="../data/vox_lib/Va.png" width="10%"/>

- $`C`$: semi-compliant voxel

<img src="../data/vox_lib/c2.png" width="10%"/>

- Ø (nothing end)

## Attributes - $`A`$
- $`A_P \in A`$
    - $`A_P = \{left, right, top, down, back, front\}`$ position in relation to parent
- $`A_R \in A`$
    - $`A_R = \{0, 90, 180, 270\}`$ rotation around mount axis
- $`Q_l \in A`$
    - joint limits (rotational or morphing)
- $`Q_i \in A`$
    - joint initial position


## Rules - $`R`$
- Rule 1 - add start voxel (controller)
```mermaid
flowchart LR
    subgraph ide0[ ]
    direction TB
        Vc-.l.->ide1[M]
        Vc-.r.->ide2[M]
        Vc-.u.->ide3[M]
        Vc-.d.->ide4[M]
        Vc-.f.->ide5[M]
        Vc-.b.->ide6[M]
    end
    id1[S] ---> ide0[ ]
```
- Rule 2 - replace mount with rigid voxel
```mermaid
flowchart LR
    subgraph ide0[ ]
    direction TB
        V-.->ide1[M]
        V-.->ide2[M]
        V-.->ide3[M]
        V-.->ide4[M]
        V-.->ide5[M]
    end
    id1[M] ---> ide0[ ]
```


- Rule 3 - replace mount with Joint
```mermaid
flowchart LR

    subgraph ide0[ ]
    direction LR
        J-.->V
        direction TB
            V-.->ide1[M]
            V-.->ide2[M]
            V-.->ide3[M]
            V-.->ide4[M]
            V-.->ide5[M]
    end
    id1[M] ---> ide0[ ]
```

- Rule 4 - replace mount with actuator voxel and semi compliant voxel
```mermaid
flowchart LR

    subgraph ide0[ ]
    direction LR
        ide1[Va]-.->ide2[C]
        ide2[C]-.->ide3[Mc]
    end
    id1[M] ---> ide0[ ]

```
- Rule 5 - add another compliant voxel tendon
```mermaid
flowchart LR
    subgraph ide0[ ]
    direction LR
        ide2[C]-.->ide3[Mc]
    end
    id1[Mc] ---> ide0[ ]
```
- Rule 6 - replace mount c with voxel
```mermaid
flowchart LR
    subgraph ide0[ ]
    direction TB
        V-.->ide1[M]
        V-.->ide2[M]
        V-.->ide3[M]
        V-.->ide4[M]
        V-.->ide5[M]
    end
    id1[Mc] ---> ide0[ ]
```
- Rule 7 - replace mount c with joint
```mermaid
flowchart LR

    subgraph ide0[ ]
    direction LR
        J-.->V
        direction TB
            V-.->ide1[M]
            V-.->ide2[M]
            V-.->ide3[M]
            V-.->ide4[M]
            V-.->ide5[M]
    end
    id1[Mc] ---> ide0[ ]
```

- Rule 8 - replace joint with elbow or wrist
```mermaid
flowchart LR

    subgraph ide0[ ]
    direction LR
        Je
    end
    subgraph ide1[ ]
    direction LR
        Jw
    end
    id2[J] ---> ide0[ ]
    id2[J] ---> ide1[ ]
```

- Rule 9 - replace mount with null (end)
```mermaid
flowchart LR

    subgraph ide0[ ]
    direction LR
        ide1[Ø]
    end
    id1[M] ---> ide0[ ]
```

- Rule 10 - replace mount c with null (end)
```mermaid
flowchart LR

    subgraph ide0[ ]
    direction LR
        ide1[Ø]
    end
    id1[Mc] ---> ide0[ ]
```

---

### Example
  - [Rover](./rover.md)

---
