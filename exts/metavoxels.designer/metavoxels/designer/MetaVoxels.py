# Copyright (c) 2022 NVIDIA CORPORATION & AFFILIATES.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
import os

import carb
import omni.kit.commands
import omni.usd
from omni.physx.scripts import physicsUtils
from pxr import Gf, UsdGeom, UsdLux, UsdPhysics

FILE_DIR = os.path.dirname(os.path.realpath(__file__))

from .MetaVoxelsLibrary import *


class MetaVoxels(omni.ext.IExt):
    def on_startup(self, ext_id):
        print("[MetaVoxels] Started")
        asyncio.ensure_future(self.create())

    async def create(self):
        await omni.kit.app.get_app_interface().next_update_async()

        Enable_Motion = True
        stage = omni.usd.get_context().get_stage()

        print("[MetaVoxels] Create demo")

        # cleanup stage
        self._stage = stage
        self._default_prim_path = stage.GetDefaultPrim().GetPath()
        children = stage.GetDefaultPrim().GetChildren()
        for child in children:
            stage.RemovePrim(child.GetPath())

        self._gravity_magnitude = 9.81  # [m/s^2]
        self._offset_amplitude = 0.3  # [m]
        self._attachment_amplitude = 0.2  # [m]
        self._motion_frequency = 0.25  # [Hz]

        self._setup_stage()

        self._create_robot()

        if Enable_Motion:
            self._time = 0.0  # motion variable tracking sim time

    def _setup_stage(self):
        # setup y-up, SI-unit stage
        UsdGeom.SetStageUpAxis(self._stage, UsdGeom.Tokens.z)
        UsdGeom.SetStageMetersPerUnit(self._stage, 1.0)
        physicsScenePath = self._default_prim_path.AppendChild("physicsScene")
        scene = UsdPhysics.Scene.Define(self._stage, physicsScenePath)
        scene.CreateGravityDirectionAttr().Set(Gf.Vec3f(0, 0, -1))
        scene.CreateGravityMagnitudeAttr().Set(self._gravity_magnitude)

        physicsUtils.add_ground_plane(
            self._stage,
            (self._default_prim_path.AppendChild("ground")).pathString,
            "Z",
            1000.00,
            Gf.Vec3f(0),
            Gf.Vec3f(1.0, 1.0, 1.0),
        )

        # setup light:
        sphereLight = UsdLux.SphereLight.Define(self._stage, self._default_prim_path.AppendChild("SphereLight"))
        sphereLight.CreateEnableColorTemperatureAttr().Set(True)
        sphereLight.CreateColorTemperatureAttr().Set(5500)
        sphereLight.CreateRadiusAttr().Set(1)
        sphereLight.CreateIntensityAttr().Set(100000.0)
        sphereLight.AddTranslateOp().Set(Gf.Vec3f(0, 15.36, 0))

        # set cam:
        customLayerData = {
            "cameraSettings": {
                "Perspective": {"position": Gf.Vec3d(0.0, 2.36, 2.49), "target": Gf.Vec3d(0.0, 0.98, -3.0)},
                "boundCamera": "/OmniverseKit_Persp",
            }
        }
        self._stage.GetRootLayer().customLayerData = customLayerData

    def _create_robot(self):
        # name unique name/id
        # link [voxel/joint , direction]
        # direction {0,1,2,3,4,5}: 0: x+, 1 x-, 2 y+, 3: y-, 4: z+, 5: z-
        # vType ={control, rigid}
        # jType = {wrist, elbow, compliant}
        # orient ={0,1} if joint is elbow or compliant 0 means the rotation is in xy plane and 1 in the zy or zx plane


        usdPath = FILE_DIR + "/../../data/USD/MetaVoxelsAbstract/"
        
        
        zoo = RobotZoo(name="metaVoxels", stage=self._stage, size=1.0, usdPath=usdPath)
        
        loadFromGraph=False
        if (loadFromGraph):
            fileName = FILE_DIR + "/../../data/graph/ant.dot"
            r = zoo.loadRobotFromGraph(fileName=fileName, name="robot", pos=Gf.Vec3d(0, 0, 1.0))
            
        else:
            
            zoo = RobotZoo(name="metaVoxels", stage=self._stage, size=1.0, usdPath=usdPath)

            r = zoo.createRover(name="robot", pos=Gf.Vec3d(0, 0, 1.0))
            # r = zoo.createAnt(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createWalker(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createSnake(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createBillE(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createBillE2(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createDog(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createGripper(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createLongRover(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.create2Handed(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.create2Handed2(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createCompliantRover(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createLoop(name="robot",pos=Gf.Vec3d(0,0,5.0))
            # r = zoo.createTL(name="robot",pos=Gf.Vec3d(0,0,5.0))
            # r = zoo.createRoverCompliantLegs(name="robot",pos=Gf.Vec3d(0,0,1.0))
            # r = zoo.createCrane(name="robot",pos=Gf.Vec3d(0,0,0.0))
