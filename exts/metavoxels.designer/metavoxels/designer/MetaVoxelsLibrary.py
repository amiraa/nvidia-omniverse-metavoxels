# Copyright (c) 2022 NVIDIA CORPORATION & AFFILIATES.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import math

import omni.kit.commands
import omni.usd
from pxr import Gf, PhysxSchema, Sdf, UsdGeom, UsdPhysics


##################################################################
class Robot:
    def __init__(self, name: str, stage, pos: Gf.Vec3d, size: float, CAD: str):
        self.name = name
        self.pos = pos
        self.size = size
        self.CAD = CAD
        self.children = {}
        self.stage = stage

        self.nameDic = {}

        self.density = float(35.0)

        self.neigh = [
            Gf.Vec3d(1, 0.0, 0.0),
            Gf.Vec3d(-1, 0.0, 0.0),
            Gf.Vec3d(0.0, 1, 0.0),
            Gf.Vec3d(0.0, -1, 0.0),
            Gf.Vec3d(0.0, 0.0, 1),
            Gf.Vec3d(0.0, 0.0, -1),
        ]

        self.create()

    def create(self):
        self.prim = UsdGeom.Xform.Define(self.stage, "/World/" + self.name)
        self.path = self.prim.GetPath()
        # UsdPhysics.ArticulationRootAPI.Apply(self.prim.GetPrim()) ## changed to control voxel for gym integration

        return self.path

    def getKey(self, loc):
        return "[" + str(int(loc[0])) + "," + str(int(loc[1])) + "," + str(int(loc[2])) + "]"

    def getNeighbors(self, vox):

        neighbors = [None, None, None, None, None, None]  # initialize with null

        i = 0
        for n in self.neigh:
            neighborName = self.getKey(vox.loc + n)
            if neighborName in self.children:
                neighbors[i] = neighborName
            i += 1

        return neighbors

    def addVoxel(self, link, vType):
        return Voxel(robotParent=self, link=link, vType=vType)

    def addJoint(self, link, jType, orient, limits):
        return Joint(robotParent=self, link=link, jType=jType, orient=orient, limits=limits)

    def addTendon(self, link, length, orient, stiffness):
        return Tendon(robotParent=self, link=link, length=length, orient=orient, stiffness=stiffness)

    def addEnd(self, link):
        return End(robotParent=self, link=link)

    def fix(self, control):
        fixedJoint = UsdPhysics.FixedJoint.Define(self.stage, self.path.AppendChild("fix_robot"))
        fixedJoint.CreateBody1Rel().SetTargets([control.path])

    def create_voxel(self, vox):
        name = vox.name

        # duplicate names
        if name in self.nameDic:
            self.nameDic[name] += 1
            name = name + str(self.nameDic[name])
            vox.name = name
        else:
            self.nameDic[name] = 0

        pos = self.pos + vox.pos
        parentPath = self.path
        rot = vox.rot
        shift = vox.shift
        scale = Gf.Vec3d(self.size, self.size, self.size)
        usdPath = vox.CAD

        voxelPath = self.stage.GetDefaultPrim().GetPath().AppendChild(name)

        # todo fix material reference to get rid of warnings
        omni.kit.commands.execute(
            "CreateReferenceCommand",
            usd_context=omni.usd.get_context(),
            path_to=voxelPath,
            asset_path=usdPath,
            instanceable=True,
        )

        omni.kit.commands.execute(
            "SetRigidBodyCommand", path=voxelPath, approximationShape="convexHull", kinematic=False
        )
        mass_api = UsdPhysics.MassAPI.Apply(self.stage.GetPrimAtPath(voxelPath))
        mass_api.CreateDensityAttr().Set(self.density)

        omni.kit.commands.execute(
            "TransformPrim",
            path=Sdf.Path(voxelPath),
            new_transform_matrix=Gf.Matrix4d(
                0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0
            ),
        )

        # rotate
        ch = "/" + self.stage.GetPrimAtPath("/World/" + name).GetChildren()[0].GetName()
        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ch + ".xformOp:rotateXYZ"),
            value=rot,
            prev=Gf.Vec3d(0.0, 0.0, 0.0),
        )
        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ch + ".xformOp:translate"),
            value=shift,
            prev=Gf.Vec3d(0.0, 0.0, 0.0),
        )
        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:rotateXYZ"),
            value=Gf.Vec3d(0.0, 0.0, 0.0),
            prev=Gf.Vec3d(0.0, 0.0, 0.0),
        )
        # scale
        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:scale"),
            value=scale,
            prev=Gf.Vec3d(1.0, 1.0, 1.0),
        )

        # move back
        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:translate"),
            value=pos,
            prev=Gf.Vec3d(0.0, 0.0, 0.0),
        )

        # move to position
        omni.kit.commands.execute(
            "MovePrim",
            path_from=self.stage.GetDefaultPrim().GetPath().AppendChild(name),
            path_to=parentPath.AppendChild(name),
        )

        return parentPath.AppendChild(name)


##################################################################
# name unique name/id
# link [voxel/joint , direction]
# direction {0,1,2,3,4,5}: 0: x+, 1 x-, 2 y+, 3: y-, 4: z+, 5: z-
# vType {control , rigid}
class Voxel:
    def __init__(self, robotParent: Robot, link, vType: str):
        self.name = vType
        self.path = ""

        self.vType = vType
        self.robotParent = robotParent
        if link[0] is None:
            self.pos = Gf.Vec3d(0.0)
            self.loc = Gf.Vec3d(0.0)
        else:
            self.pos = link[0].neighborsPos[link[1]]
            self.loc = link[0].neighborsLoc[link[1]]

        self.size = self.robotParent.size
        self.rot = Gf.Vec3d(0, 0, 0.0)
        self.shift = Gf.Vec3d(0, 0, 0.0)

        # todo add robot position

        # up, down, right, left, back, front
        self.neighborsPos = [
            self.pos + Gf.Vec3d(self.size, 0.0, 0.0),
            self.pos + Gf.Vec3d(-self.size, 0.0, 0.0),
            self.pos + Gf.Vec3d(0.0, self.size, 0.0),
            self.pos + Gf.Vec3d(0.0, -self.size, 0.0),
            self.pos + Gf.Vec3d(0.0, 0.0, self.size),
            self.pos + Gf.Vec3d(0.0, 0.0, -self.size),
        ]

        self.neighborsLoc = [
            self.loc + Gf.Vec3d(1, 0.0, 0.0),
            self.loc + Gf.Vec3d(-1, 0.0, 0.0),
            self.loc + Gf.Vec3d(0.0, 1, 0.0),
            self.loc + Gf.Vec3d(0.0, -1, 0.0),
            self.loc + Gf.Vec3d(0.0, 0.0, 1),
            self.loc + Gf.Vec3d(0.0, 0.0, -1),
        ]

        # add to scene
        self.CAD = self.robotParent.CAD + vType + ".usda"
        self.create()

        # get neighbors
        self.neighbors = self.robotParent.getNeighbors(self)

        # add fixed joints to neighbors
        self.fixVoxel()

    def create(self):
        print(f"created voxel at {self.pos[0]},{self.pos[1]},{self.pos[2]}!")
        self.path = self.robotParent.create_voxel(self)
        self.robotParent.children[self.robotParent.getKey(self.loc)] = ["v", self.path]
        if self.vType == "control":

            UsdPhysics.ArticulationRootAPI.Apply(
                self.robotParent.stage.GetPrimAtPath(self.path)
            )  ## ad articulation root for gym control

    def fixVoxel(self):
        i = 0
        for n in self.neighbors:
            if n is not None:
                size = self.robotParent.size
                # add fixed joint
                joint = UsdPhysics.FixedJoint.Define(
                    self.robotParent.stage, self.path.AppendChild("fixedJoint_" + self.name + "_" + str(i))
                )

                joint.CreateBody0Rel().SetTargets([self.path])
                if self.robotParent.children[n][0] == "j":
                    size = self.robotParent.children[n][3]
                    if i % 2 != 0:
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                    else:
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0.0, 0.0))
                elif self.robotParent.children[n][0] == "c":
                    joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0.0, 0.5))
                else:
                    joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0.0, 0.0))

                joint.CreateLocalRot0Attr().Set(Gf.Quatf(0.0))
                joint.CreateLocalPos1Attr().Set(-size * self.robotParent.neigh[i])
                joint.CreateLocalRot1Attr().Set(Gf.Quatf(0.0))
            i += 1

    def setNeighbor(self, index, neighbor):
        self.neighbors[index] = neighbor


##################################################################

# name unique name/id
# link [voxel/joint , direction]
# direction {0,1,2,3,4,5}: 0: x+, 1 x-, 2 y+, 3: y-, 4: z+, 5: z-
# jType = {wrist, elbow, compliant}
# orient ={0,1} if joint is elbow or compliant 0 means the rotation is in xy plane and 1 in the zy or zx plane
class Joint:
    def __init__(self, robotParent: Robot, link, jType: str, orient: int, limits):
        self.name = jType
        self.path = ""

        self.jType = jType
        self.robotParent = robotParent

        self.pos = link[0].neighborsPos[link[1]]
        self.loc = link[0].neighborsLoc[link[1]]
        self.link = link

        self.size = self.robotParent.size
        self.shift = Gf.Vec3d(0, 0, 0.0)

        self.orient = orient

        self.rot = Gf.Vec3d(0, 0, 0.0)

        self.wristSize = 0.1

        if orient == 0:
            self.rot = Gf.Vec3d(0.0, 0, 90)

        if link[1] == 2 or link[1] == 3:
            self.rot = Gf.Vec3d(-90.0, 0, 0)
            if orient == 1:
                self.rot = Gf.Vec3d(-90.0, 90, 0)
            self.shift = Gf.Vec3d(0, -self.size / 2.0, self.size / 2.0)

        if link[1] == 0 or link[1] == 1:
            self.rot = Gf.Vec3d(0.0, 90, 0)
            if orient == 0:
                self.rot = Gf.Vec3d(90.0, 0, 90)
            self.shift = Gf.Vec3d(-self.size / 2.0, 0, self.size / 2.0)

        if self.jType == "wrist":
            if link[1] == 3:
                self.shift = Gf.Vec3d(0, self.size / 2.0 - self.wristSize, self.size / 2.0)
            elif link[1] == 1:
                self.shift = Gf.Vec3d(self.size / 2.0 - self.wristSize, 0, self.size / 2.0)
            elif link[1] == 5:
                self.shift = Gf.Vec3d(0, 0, self.size - self.wristSize)

        if self.jType == "wrist":
            self.size = self.wristSize

        # todo edit to only reflect real neighbors
        self.neighborsPos = [
            self.pos + Gf.Vec3d(self.size, 0.0, 0.0),
            self.pos + Gf.Vec3d(-self.size, 0.0, 0.0),
            self.pos + Gf.Vec3d(0.0, self.size, 0.0),
            self.pos + Gf.Vec3d(0.0, -self.size, 0.0),
            self.pos + Gf.Vec3d(0.0, 0.0, self.size),
            self.pos + Gf.Vec3d(0.0, 0.0, -self.size),
        ]

        self.neighborsLoc = [
            self.loc + Gf.Vec3d(1, 0.0, 0.0),
            self.loc + Gf.Vec3d(-1, 0.0, 0.0),
            self.loc + Gf.Vec3d(0.0, 1, 0.0),
            self.loc + Gf.Vec3d(0.0, -1, 0.0),
            self.loc + Gf.Vec3d(0.0, 0.0, 1),
            self.loc + Gf.Vec3d(0.0, 0.0, -1),
        ]

        self.limits = limits
        self.CAD1 = self.robotParent.CAD + jType + "_1.usda"
        self.CAD2 = self.robotParent.CAD + jType + "_2.usda"
        self.create()

        # get neighbors
        self.neighbors = self.robotParent.getNeighbors(self)

        # add fixed joints to neighbors
        self.fixJoint()

    def create(self):
        print(f"created joint at ${self.pos[0]},${self.pos[1]},${self.pos[2]}!")

        self.CAD = self.CAD1
        path1 = self.robotParent.create_voxel(self)

        self.CAD = self.CAD2
        self.name = self.name + "_1"
        path2 = self.robotParent.create_voxel(self)

        self.path = [path1, path2]

        if self.jType == "elbow":
            self.robotParent.children[self.robotParent.getKey(self.loc)] = ["j", path1, path2, self.size]
        else:
            self.robotParent.children[self.robotParent.getKey(self.loc)] = ["j", path1, path2, self.size]

        joint = UsdPhysics.RevoluteJoint.Define(self.robotParent.stage, path1.AppendChild("revoluteJoint_" + self.name))

        joint.CreateBody0Rel().SetTargets([path1])
        joint.CreateBody1Rel().SetTargets([path2])

        joint.CreateLowerLimitAttr(float(self.limits[0]))
        joint.CreateUpperLimitAttr(float(self.limits[1]))

        if self.jType == "elbow":
            if self.link[1] == 0 or self.link[1] == 1:
                if self.orient == 0:
                    joint.CreateAxisAttr("Z")

                else:
                    joint.CreateAxisAttr("Y")
            elif self.link[1] == 2 or self.link[1] == 3:
                if self.orient == 1:
                    joint.CreateAxisAttr("X")

                else:
                    joint.CreateAxisAttr("Z")
            else:
                if self.orient == 0:
                    joint.CreateAxisAttr("X")

                else:
                    joint.CreateAxisAttr("Y")
            joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0, 0.5))
            joint.CreateLocalPos1Attr().Set(Gf.Vec3d(0.0, 0, 0.5))

        if self.jType == "wrist":
            if self.link[1] == 0 or self.link[1] == 1:
                joint.CreateAxisAttr("X")
                if self.link[1] == 0:
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(-0.5, 0, 0.5))
                    joint.CreateLocalPos1Attr().Set(Gf.Vec3d(-0.5, 0, 0.5))
                else:
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.5, 0, 0.5))
                    joint.CreateLocalPos1Attr().Set(Gf.Vec3d(0.5, 0, 0.5))

            elif self.link[1] == 2 or self.link[1] == 3:
                joint.CreateAxisAttr("Y")
                if self.link[1] == 2:
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, -0.5, 0.5))
                    joint.CreateLocalPos1Attr().Set(Gf.Vec3d(0.0, -0.5, 0.5))
                else:
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0.5, 0.5))
                    joint.CreateLocalPos1Attr().Set(Gf.Vec3d(0.0, 0.5, 0.5))

            else:
                joint.CreateAxisAttr("Z")
                if self.link[1] == 4:
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0, 0.0))
                    joint.CreateLocalPos1Attr().Set(Gf.Vec3d(0.0, 0, 0.0))
                else:
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0, 1))
                    joint.CreateLocalPos1Attr().Set(Gf.Vec3d(0.0, 0, 1))

        joint.CreateLocalRot0Attr().Set(Gf.Quatf(0.0))
        joint.CreateLocalRot1Attr().Set(Gf.Quatf(0.0))

    def fixJoint(self):
        i = 0
        for n in self.neighbors:
            if n is not None:
                if i % 2 != 0:
                    # add fixed joint
                    joint = UsdPhysics.FixedJoint.Define(
                        self.robotParent.stage, self.path[0].AppendChild("fixedJoint_" + self.name + "_" + str(i))
                    )

                    joint.CreateBody0Rel().SetTargets([self.path[0]])
                    if self.robotParent.children[n][0] == "j":
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                    elif self.robotParent.children[n][0] == "c":
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                    else:
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])
                else:
                    # add fixed joint
                    joint = UsdPhysics.FixedJoint.Define(
                        self.robotParent.stage, self.path[1].AppendChild("fixedJoint_" + self.name + "_" + str(i))
                    )

                    joint.CreateBody0Rel().SetTargets([self.path[1]])
                    if self.robotParent.children[n][0] == "j":
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])
                    elif self.robotParent.children[n][0] == "c":
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                    else:
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])

                joint.CreateLocalPos0Attr().Set(self.robotParent.neigh[i])
                joint.CreateLocalRot0Attr().Set(Gf.Quatf(0.0))
                joint.CreateLocalPos1Attr().Set(Gf.Vec3d(0.0, 0, 0.0))
                joint.CreateLocalRot1Attr().Set(Gf.Quatf(0.0))

            i += 1

    def setNeighbor(self, index, neighbor):  # only 0 and 1
        self.neighbors[index] = neighbor


##################################################################
# name unique name/id
# link [voxel/joint , direction]
# direction {0,1,2,3,4,5}: 0: x+, 1 x-, 2 y+, 3: y-, 4: z+, 5: z-
# length = how many elements
# orient =[{0,1},{0,1}]
#   first if joint is elbow or compliant 0 means the rotation is in xy plane and 1 in the zy or zx plane
#   second up or down
class Tendon:
    def __init__(self, robotParent: Robot, link, length: int, orient: tuple, stiffness: float):
        self.name = "tendon"

        # duplicate names
        if self.name in robotParent.nameDic:
            robotParent.nameDic[self.name] += 1
            self.name = self.name + str(robotParent.nameDic[self.name])
        else:
            robotParent.nameDic[self.name] = 0

        self.path = ""

        self.robotParent = robotParent
        self.link = link

        self.pos = link[0].neighborsPos[link[1]]
        self.loc = link[0].neighborsLoc[link[1]]
        self.link = link
        self.length = length

        self.size = self.robotParent.size
        self.shift = Gf.Vec3d(0, 0, 0.0)

        self.orient = orient
        if self.orient[1] == 0:  # up or down
            self.tension1 = 1 / 3.0
            self.tension2 = 1.0
        else:
            self.tension2 = 1 / 3.0
            self.tension1 = 1.0

        self.stiffnessScale = 1 / 0.01 * stiffness

        self.rot = Gf.Vec3d(0, 0, 0.0)

        self._gravity_magnitude = 9.81  # [m/s^2]
        self._offset_amplitude = 0.3  # [m]
        self._attachment_amplitude = 0.2  # [m]
        self._motion_frequency = 0.25  # [Hz]
        self._link_dimensions = Gf.Vec3d(1.0, 0.05, 0.05)  # [m]
        self._link_density = 1000.0  # [kg/m^3]

        # link colors
        self._staticColor = Gf.Vec3d(165.0 / 255.0, 21.0 / 255.0, 21.0 / 255.0)
        self._dynamicColor = Gf.Vec3d(71.0 / 255.0, 165.0 / 255.0, 1.0)

        # todo edit to only reflect real neighbors
        self.neighborsPos = [
            self.pos + Gf.Vec3d(self.size, 0.0, 0.0) * (length + 1.0),
            self.pos + Gf.Vec3d(-self.size, 0.0, 0.0) * (length + 1.0),
            self.pos + Gf.Vec3d(0.0, self.size, 0.0) * (length + 1.0),
            self.pos + Gf.Vec3d(0.0, -self.size, 0.0) * (length + 1.0),
            self.pos + Gf.Vec3d(0.0, 0.0, self.size) * (length + 1.0),
            self.pos + Gf.Vec3d(0.0, 0.0, -self.size) * (length + 1.0),
        ]

        self.neighborsLoc = [
            self.loc + Gf.Vec3d(1, 0.0, 0.0) * (length + 1.0),
            self.loc + Gf.Vec3d(-1, 0.0, 0.0) * (length + 1.0),
            self.loc + Gf.Vec3d(0.0, 1, 0.0) * (length + 1.0),
            self.loc + Gf.Vec3d(0.0, -1, 0.0) * (length + 1.0),
            self.loc + Gf.Vec3d(0.0, 0.0, 1) * (length + 1.0),
            self.loc + Gf.Vec3d(0.0, 0.0, -1) * (length + 1.0),
        ]

        self.usdPath = self.robotParent.CAD + "../tendonAbstract/voxel_sides_1.usda"
        self.usdPath_b = self.robotParent.CAD + "../tendonAbstract/voxel_sides_b.usda"
        self.usdPath_c = self.robotParent.CAD + "../tendonAbstract/compliant_face_half.usda"

        self.create()

        # get neighbors
        self.neighbors = self.robotParent.getNeighbors(self)

        # add fixed joints to neighbors
        self.fixVoxel()

    def create(self):
        self.path = self.robotParent.path.AppendChild(self.name)
        apis = self._create_articulation(
            self.name, self.robotParent.pos + self.pos + Gf.Vec3d(0.0, 0.0, self.size / 2.0)
        )
        self._offset_attachment = apis[0]
        self.robotParent.children[self.robotParent.getKey(self.loc)] = [
            "c",
            self.path,
            self.path.AppendChild(self.name + "_link_" + str(self.length - 1)),
        ]
        for i in range(self.length):
            self.robotParent.children[
                self.robotParent.getKey(self.loc + self.robotParent.neigh[self.link[1]] * (i + 1))
            ] = ["c", self.path, self.path.AppendChild(self.name + "_link_" + str(self.length - 1))]

    def _create_articulation(self, articulation_name: str, offset: Gf.Vec3d):
        basePath = self.path.AppendChild(articulation_name + "_BaseLink")
        base = UsdGeom.Xform.Define(self.robotParent.stage, basePath)
        base.AddTranslateOp().Set(offset)
        base.AddScaleOp().Set(self._link_dimensions)

        UsdPhysics.ArticulationRootAPI.Apply(base.GetPrim())

        self.Crot = Gf.Vec3d(0, 90.0, -90)
        self.Crot_c = Gf.Vec3d(0, 90.0, 0)
        self.Crot_ct = Gf.Vec3d(0, -90.0, 0)
        # todo make this parametric according to link size
        self.Cscale = Gf.Vec3d(0.15, 0.15, 0.15)
        self.Cscalet = Gf.Vec3d(0.15, -0.15, 0.15)

        prim = UsdGeom.Xform.Define(self.robotParent.stage, basePath.AppendChild("CAD"))
        basePath1 = basePath.AppendChild("CAD")

        self.list_compliant_paths_t = []

        voxPath = self._create_voxel(
            usdPath=self.usdPath_b,
            name="voxel_base",
            parentPath=basePath1,
            pos=offset,
            rot=self.Crot,
            scale=self.Cscale,
        )
        compliantPath = self._create_voxel(
            usdPath=self.usdPath_c,
            name="compliant_base_b",
            parentPath=basePath1,
            pos=offset,
            rot=self.Crot_c,
            scale=self.Cscale,
        )
        compliantPath = self._create_voxel(
            usdPath=self.usdPath_c,
            name="compliant_base_t",
            parentPath=basePath1,
            pos=offset,
            rot=self.Crot_ct,
            scale=self.Cscalet,
        )
        self.list_compliant_paths_t.append(compliantPath)
        joint_offset = Gf.Vec3d(0.5, 0, 0)

        self.fixScaleCAD(prim, basePath1)

        omni.kit.commands.execute(
            "SetRigidBodyCommand", path=base.GetPath(), approximationShape="convexHull", kinematic=False
        )

        for i in range(self.length):
            # position is 0.5 because link shape is achieved via scaling that needs to be considered in the joint pos
            if self.link[1] == 0:
                link_offset = Gf.Vec3d(self._link_dimensions[0] * (i + 1), 0, 0)
            elif self.link[1] == 1:
                link_offset = Gf.Vec3d(-self._link_dimensions[0] * (i + 1), 0, 0)

            right_link_path = self.path.AppendChild(articulation_name + "_link_" + str(i))
            self._create_moving_link(
                link_path=right_link_path,
                parent_path=basePath,
                translation=offset + link_offset,
                parent_joint_offset=joint_offset,
                order=i,
            )
            self._setup_spatial_tendon(num=i, base_link_path=basePath, right_link_path=right_link_path)
            basePath = right_link_path

        return [0, 0]

    def _create_moving_link(
        self,
        link_path: Sdf.Path,
        parent_path: Sdf.Path,
        translation: Gf.Vec3d,
        parent_joint_offset: Gf.Vec3d,
        order: int,
    ):
        left_link = UsdGeom.Xform.Define(self.robotParent.stage, link_path)

        left_link.AddTranslateOp().Set(translation)
        left_link.AddScaleOp().Set(self._link_dimensions)

        prim = UsdGeom.Xform.Define(self.robotParent.stage, link_path.AppendChild("CAD"))
        right_link_path = link_path.AppendChild("CAD")

        voxPath = self._create_voxel(
            usdPath=self.usdPath,
            name="voxel_link_" + str(order),
            parentPath=right_link_path,
            pos=translation,
            rot=self.Crot,
            scale=self.Cscale,
        )
        compliantPath = self._create_voxel(
            usdPath=self.usdPath_c,
            name="compliant_link_b_" + str(order) + "_",
            parentPath=right_link_path,
            pos=translation,
            rot=self.Crot_c,
            scale=self.Cscale,
        )
        compliantPath = self._create_voxel(
            usdPath=self.usdPath_c,
            name="compliant_link_t_" + str(order) + "_",
            parentPath=right_link_path,
            pos=translation,
            rot=self.Crot_ct,
            scale=self.Cscalet,
        )
        self.list_compliant_paths_t.append(compliantPath)

        self.fixScaleCAD(prim, right_link_path)

        ##################

        omni.kit.commands.execute(
            "SetRigidBodyCommand", path=link_path, approximationShape="convexHull", kinematic=False
        )

        mass_api = UsdPhysics.MassAPI.Apply(prim.GetPrim())
        mass_api.CreateDensityAttr().Set(self._link_density)

        joint = UsdPhysics.RevoluteJoint.Define(
            self.robotParent.stage, link_path.AppendChild("revoluteJoint_" + self.name)
        )
        joint.CreateBody0Rel().SetTargets([parent_path])
        joint.CreateBody1Rel().SetTargets([link_path])
        joint.CreateAxisAttr("Z")
        joint.CreateLocalPos0Attr().Set(parent_joint_offset)
        joint.CreateLocalRot0Attr().Set(Gf.Quatf(0.0))
        joint.CreateLocalPos1Attr().Set(-parent_joint_offset)
        joint.CreateLocalRot1Attr().Set(Gf.Quatf(0.0))

    def fixScaleCAD(self, prim, path):
        prim.AddScaleOp().Set(Gf.Vec3f(2.25, 2.25, 2.25))

    def _create_voxel(
        self, usdPath: str, name: str, parentPath: Sdf.Path, pos: Gf.Vec3d, rot: Gf.Vec3d, scale: Gf.Vec3d
    ):
        # #todo change absolute path
        voxelPath = self.robotParent.stage.GetDefaultPrim().GetPath().AppendChild(name)

        omni.kit.commands.execute(
            "CreateReferenceCommand",
            usd_context=omni.usd.get_context(),
            path_to=voxelPath,
            asset_path=usdPath,
            instanceable=True,
        )

        omni.kit.commands.execute(
            "TransformPrim",
            path=Sdf.Path(voxelPath),
            new_transform_matrix=Gf.Matrix4d(
                0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0
            ),
        )

        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:rotateXYZ"),
            value=rot,
            prev=Gf.Vec3d(0.0, 0.0, 0.0),
        )

        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:scale"),
            value=scale,
            prev=Gf.Vec3d(1.0, 1.0, 1.0),
        )

        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:translate"),
            value=pos,
            prev=Gf.Vec3d(0.0, 0.0, 0.0),
        )

        omni.kit.commands.execute(
            "MovePrim",
            path_from=self.robotParent.stage.GetDefaultPrim().GetPath().AppendChild(name),
            path_to=parentPath.AppendChild(name),
        )

        return voxelPath

    def _setup_spatial_tendon(self, num: int, base_link_path: Sdf.Path, right_link_path: Sdf.Path):
        """
        Sets up a spatial tendon with a root attachment on the left link, a regular attachment on the base, and a
        leaf attachment on the right link.

        The root and leaf attachment are at the center of gravity of the links, and the base link's attachment is
        a link half-length above the fixed-base's COG in the stage-up direction (+Y).

        The code sizes the tendon's stiffness based on the tendon geometry and link mass
        """
        base_prim = self.robotParent.stage.GetPrimAtPath(base_link_path)
        right_link_prim = self.robotParent.stage.GetPrimAtPath(right_link_path)

        half_length = self._link_dimensions[0] * 0.5

        # Tendon stiffness dimensioning
        # We could set the rest length to -1 to autocompute it but we calculate it here to size the tendon stiffness
        # based on a desired deviation from the rest length
        # the rest length is 2 * sqrt((2 * half_length)^2 + half_length^2) = 2 * half_length * sqrt(5)
        rest_length = 2.0 * half_length * math.sqrt(5.0)
        rest_length = self._link_dimensions[0]

        # The goal is to have the revolute joints deviate just a few degrees from the horizontal
        # and we compute the length of the tendon at that angle:
        deviationAngle = 3.0 * math.pi / 180.0
        # Distances from the base-link attachment to the root and leaf attachments
        vertical_distance = half_length * (1.0 + math.sin(deviationAngle))
        horizontal_distance = half_length * (1.0 + math.cos(deviationAngle))
        deviated_length = 2.0 * math.sqrt(vertical_distance**2.0 + horizontal_distance**2.0)

        # At rest, the force on the leaf attachment is (deviated_length - rest_length) * tendon_stiffness.
        # The force points from the moving links' COG to the fixed-base attachment.
        # The force needs to be equal to the gravity force acting on the link, projected onto the tendon.
        # An equal and opposing (in the direction of the tendon) force acts on the root-attachment link and will hold
        # that link up. In order to calculate the tendon stiffness that produces that force, we consider the forces
        # and attachment geometry at zero joint angles.
        link_mass = self._link_dimensions[0] * self._link_dimensions[1] * self._link_dimensions[2] * self._link_density
        gravity_force = self._gravity_magnitude * link_mass
        # Project onto tendon at rest length / with joints at zero angle
        tendon_force = gravity_force * 0.5 * rest_length / half_length
        # and compute stiffness to get tendon force at deviated length to hold the link:
        tendon_stiffness = tendon_force / (deviated_length - rest_length)

        tendon_stiffness = tendon_force * self.stiffnessScale
        tendon_damping = 0.3 * tendon_stiffness

        attachment_local_pos = half_length / self._link_dimensions[1]

        rootApi = PhysxSchema.PhysxTendonAttachmentRootAPI.Apply(base_prim, "Root_t_" + str(num))
        # center attachment above dynamic link
        rootApi.CreateLocalPosAttr().Set(Gf.Vec3d(0.5, attachment_local_pos, 0.0))
        rootApi.CreateStiffnessAttr().Set(tendon_stiffness)
        rootApi.CreateDampingAttr().Set(tendon_damping)

        attachmentApi = PhysxSchema.PhysxTendonAttachmentAPI.Apply(right_link_prim, "Attachment_t_" + str(num))
        attachmentApi.CreateParentLinkRel().AddTarget(base_link_path)
        attachmentApi.CreateParentAttachmentAttr().Set("Root_t_" + str(num))
        # the attachment local pos takes the link scaling into account, just like the joint positions
        attachmentApi.CreateLocalPosAttr().Set(Gf.Vec3d(0.0, attachment_local_pos, 0.0))

        leafApi = PhysxSchema.PhysxTendonAttachmentLeafAPI.Apply(right_link_prim, "Leaf_t_" + str(num))
        leafApi.CreateParentLinkRel().AddTarget(right_link_path)
        leafApi.CreateParentAttachmentAttr().Set("Attachment_t_" + str(num))
        leafApi.CreateLocalPosAttr().Set(Gf.Vec3d(0.5, attachment_local_pos, 0.0))
        leafApi.CreateRestLengthAttr().Set(rest_length * self.tension1)

        # bottom
        rootApi_1 = PhysxSchema.PhysxTendonAttachmentRootAPI.Apply(base_prim, "Root_b_" + str(num))
        # center attachment above dynamic link
        rootApi_1.CreateLocalPosAttr().Set(Gf.Vec3d(0.5, -attachment_local_pos, 0.0))
        rootApi_1.CreateStiffnessAttr().Set(tendon_stiffness)
        rootApi_1.CreateDampingAttr().Set(tendon_damping)

        attachmentApi_1 = PhysxSchema.PhysxTendonAttachmentAPI.Apply(right_link_prim, "Attachment_b_" + str(num))
        attachmentApi_1.CreateParentLinkRel().AddTarget(base_link_path)
        attachmentApi_1.CreateParentAttachmentAttr().Set("Root_b_" + str(num))
        # the attachment local pos takes the link scaling into account, just like the joint positions
        attachmentApi_1.CreateLocalPosAttr().Set(Gf.Vec3d(0.0, -attachment_local_pos, 0.0))

        leafApi_1 = PhysxSchema.PhysxTendonAttachmentLeafAPI.Apply(right_link_prim, "Leaf_b_" + str(num))
        leafApi_1.CreateParentLinkRel().AddTarget(right_link_path)
        leafApi_1.CreateParentAttachmentAttr().Set("Attachment_b_" + str(num))
        leafApi_1.CreateLocalPosAttr().Set(Gf.Vec3d(0.5, -attachment_local_pos, 0.0))
        leafApi_1.CreateRestLengthAttr().Set(rest_length * self.tension2)

        return (rootApi, attachmentApi, leafApi)

    def fixVoxel(self):
        i = 0
        for n in self.neighbors:
            if n is not None:
                size = self.robotParent.size
                # add fixed joint
                joint = UsdPhysics.FixedJoint.Define(
                    self.robotParent.stage, self.path.AppendChild("fixedJoint_" + self.name)
                )

                joint.CreateBody0Rel().SetTargets([self.path.AppendChild(self.name + "_BaseLink")])

                if self.robotParent.children[n][0] == "j":
                    size = self.robotParent.children[n][3]
                    if i % 2 != 0:
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                    else:
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])
                elif self.robotParent.children[n][0] == "c":
                    joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                else:
                    joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])

                joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0.0, -10.0))
                joint.CreateLocalRot0Attr().Set(Gf.Quatf(0.0))
                if self.link[1] == 0:
                    joint.CreateLocalPos1Attr().Set(-size * self.robotParent.neigh[i])
                elif self.link[1] == 1:
                    joint.CreateLocalPos1Attr().Set(size * self.robotParent.neigh[i])

                joint.CreateLocalRot1Attr().Set(Gf.Quatf(0.0))
            i += 1


##################################################################
# class end
class End:
    def __init__(self, robotParent: Robot, link):
        self.name = "end"
        self.path = ""

        self.robotParent = robotParent

        self.pos = link[0].neighborsPos[link[1]]
        self.loc = link[0].neighborsLoc[link[1]]
        self.link = link

        self.size = self.robotParent.size
        self.shift = Gf.Vec3d(0, 0, 0.0)

        self.rot = Gf.Vec3d(0, 0, 0.0)

        if link[1] == 0:
            self.rot = Gf.Vec3d(0.0, 90, 0)
            self.shift = Gf.Vec3d(-self.size / 2.0, 0, self.size / 2.0)
        elif link[1] == 1:
            self.rot = Gf.Vec3d(0.0, 90, 180)
            self.shift = Gf.Vec3d(self.size / 2.0, 0, self.size / 2.0)
        elif link[1] == 2:
            self.rot = Gf.Vec3d(-90.0, 0, 0)
            self.shift = Gf.Vec3d(0, -self.size / 2.0, self.size / 2.0)
        elif link[1] == 3:
            self.rot = Gf.Vec3d(-90.0, 0, 180)
            self.shift = Gf.Vec3d(0, self.size / 2.0, self.size / 2.0)
        elif link[1] == 5:
            self.rot = Gf.Vec3d(0, 180, 0.0)
            self.shift = Gf.Vec3d(0, 0, self.size)

        self.neighborsPos = [
            self.pos + Gf.Vec3d(self.size, 0.0, 0.0),
            self.pos + Gf.Vec3d(-self.size, 0.0, 0.0),
            self.pos + Gf.Vec3d(0.0, self.size, 0.0),
            self.pos + Gf.Vec3d(0.0, -self.size, 0.0),
            self.pos + Gf.Vec3d(0.0, 0.0, self.size),
            self.pos + Gf.Vec3d(0.0, 0.0, -self.size),
        ]

        self.neighborsLoc = [
            self.loc + Gf.Vec3d(1, 0.0, 0.0),
            self.loc + Gf.Vec3d(-1, 0.0, 0.0),
            self.loc + Gf.Vec3d(0.0, 1, 0.0),
            self.loc + Gf.Vec3d(0.0, -1, 0.0),
            self.loc + Gf.Vec3d(0.0, 0.0, 1),
            self.loc + Gf.Vec3d(0.0, 0.0, -1),
        ]

        self.CAD = self.robotParent.CAD + "end.usda"
        print(self.CAD)
        self.create()

        # get neighbors
        self.neighbors = self.robotParent.getNeighbors(self)

        # add fixed joints to neighbors
        self.fixJoint()

    def create(self):
        print(f"created end at ${self.pos[0]},${self.pos[1]},${self.pos[2]}!")

        self.path = self.robotParent.create_voxel(self)

        self.robotParent.children[self.robotParent.getKey(self.loc)] = ["e", self.path]

        # #todo create force sensor
        sensorAPI = PhysxSchema.PhysxArticulationForceSensorAPI.Apply(self.robotParent.stage.GetPrimAtPath(self.path))
        sensorAPI.CreateConstraintSolverForcesEnabledAttr().Set(True)
        sensorAPI.CreateForwardDynamicsForcesEnabledAttr().Set(True)

    def fixJoint(self):
        i = 0
        for n in self.neighbors:
            if n is not None:
                size = self.robotParent.size
                # add fixed joint
                joint = UsdPhysics.FixedJoint.Define(
                    self.robotParent.stage, self.path.AppendChild("fixedJoint_" + self.name + "_" + str(i))
                )

                joint.CreateBody0Rel().SetTargets([self.path])
                if self.robotParent.children[n][0] == "j":
                    size = self.robotParent.children[n][3]
                    if i % 2 != 0:
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                    else:
                        joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0.0, 0.0))
                elif self.robotParent.children[n][0] == "c":
                    joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][2]])
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0.0, 0.5))
                else:
                    joint.CreateBody1Rel().SetTargets([self.robotParent.children[n][1]])
                    joint.CreateLocalPos0Attr().Set(Gf.Vec3d(0.0, 0.0, 0.0))

                joint.CreateLocalRot0Attr().Set(Gf.Quatf(0.0))
                joint.CreateLocalPos1Attr().Set(-size * self.robotParent.neigh[i])
                joint.CreateLocalRot1Attr().Set(Gf.Quatf(0.0))
            i += 1

    def setNeighbor(self, index, neighbor):  # only 0 and 1
        self.neighbors[index] = neighbor


##################################################################
class RobotZoo:
    def __init__(self, name: str, stage, size: float, usdPath: str):
        self._stage = stage
        self.usdPath = usdPath
        self.size = size


    #GRAPH
    def loadRobotFromGraph(self,fileName: str,name: str,  pos):
        #load graph libraries
        from omni.kit.pipapi import install
        omni.kit.pipapi.install('networkx')
        omni.kit.pipapi.install('graphviz')
        omni.kit.pipapi.install('pydot')
        import pydot
        import networkx as nx
        import graphviz

        graphs = pydot.graph_from_dot_file(fileName)
        graph = graphs[0]
        graph
        G = nx.drawing.nx_pydot.from_pydot(graph)
        G.remove_node('\\n')
        print(G)
        nodes=list(G.nodes)
        edges=list(G.edges)
        label = nx.get_node_attributes(G, "label")
        # print(label)
        orients = nx.get_node_attributes(G, "orient")
        # print(orient)
        limMin = nx.get_node_attributes(G, "limMin")
        # print(limMin)
        limMax = nx.get_node_attributes(G, "limMax")
        # print(limMax)
        elabel = nx.get_edge_attributes(G, "label")
        # print(elabel)
        Vc="v"
        bfs=list(nx.bfs_edges(G, source=Vc))
        partsDicts={}
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)
        # print('r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)')
        partsDicts[Vc]=r.addVoxel(link=[None, 0], vType="control")
        # partsDicts[Vc]='r.addVoxel(link=[None, 0], vType="control")'
        print(Vc,"=",partsDicts[Vc])
        for t in bfs:
            
            dir=int(elabel[(t[0], t[1], 0)])
            nei=partsDicts[t[0]]
            vType=label[t[1]].replace('"', "")

            if vType=="rigid":
                partsDicts[t[1]]=r.addVoxel(link=[nei, dir], vType=vType)
                # partsDicts[t[1]]= 'r.addVoxel(link=['+t[0]+', '+str(dir)+'], vType='+vType+')'
                # print(t[1],"=",partsDicts[t[1]])
            elif vType=="elbow" or vType=="wrist":

                partsDicts[t[1]]=r.addJoint(link=[nei, dir], jType=vType, orient=int(orients[t[1]]), limits=[float(limMin[t[1]]), float(limMax[t[1]])])
                # partsDicts[t[1]]= 'r.addJoint(link=['+t[0]+', '+str(dir)+'], vType='+vType+',orient='+ orient[t[1]]    +',limits=['+ limMin[t[1]]  +', '+ limMax[t[1]] +'])'
                # print(t[1],"=",partsDicts[t[1]])
            elif vType=="end":
                partsDicts[t[1]]=r.addEnd(link=[nei, dir])
                # partsDicts[t[1]]= 'r.addEnd(link=['+t[0]+', '+str(dir)+'])'
                # print(t[1],"=",partsDicts[t[1]])
        return r


    def createAnt(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        j2 = r.addJoint(link=[v, 0], jType="elbow", orient=0, limits=[-50, 50])
        j3 = r.addJoint(link=[v, 1], jType="elbow", orient=0, limits=[-50, 50])
        j4 = r.addJoint(link=[v, 2], jType="elbow", orient=0, limits=[-50, 50])
        j5 = r.addJoint(link=[v, 3], jType="elbow", orient=0, limits=[-50, 50])

        j6 = r.addJoint(link=[j2, 0], jType="elbow", orient=1, limits=[30, 100])
        j7 = r.addJoint(link=[j3, 1], jType="elbow", orient=1, limits=[30, 100])
        j8 = r.addJoint(link=[j4, 2], jType="elbow", orient=1, limits=[-100, -30])
        j9 = r.addJoint(link=[j5, 3], jType="elbow", orient=1, limits=[-100, -30])

        v4 = r.addVoxel(link=[j6, 0], vType="rigid")
        v5 = r.addVoxel(link=[j7, 1], vType="rigid")
        v6 = r.addVoxel(link=[j8, 2], vType="rigid")
        v7 = r.addVoxel(link=[j9, 3], vType="rigid")

        e1 = r.addEnd(link=[v4, 0])
        e2 = r.addEnd(link=[v5, 1])
        e3 = r.addEnd(link=[v6, 2])
        e4 = r.addEnd(link=[v7, 3])

        # prepare for training

        # rotate (y z up switch ) and scale for gym training
        r.prim.AddScaleOp().Set(Gf.Vec3d(0.25, 0.25, 0.25))
        m = UsdGeom.Xform.Define(r.stage, "/World/" + "metavoxels")

        omni.kit.commands.execute("MovePrim", path_from=r.path, path_to=Sdf.Path("/World/" + "metavoxels/" + r.name))

        return r

    def createRover(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        v1 = r.addVoxel(link=[v, 2], vType="rigid")

        j1 = r.addJoint(link=[v1, 2], jType="elbow", orient=0, limits=(-180, 180))

        v2 = r.addVoxel(link=[j1, 2], vType="rigid")
        v3 = r.addVoxel(link=[v2, 2], vType="rigid")

        j2 = r.addJoint(link=[v1, 0], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v1, 1], jType="elbow", orient=1, limits=(-180, 180))
        j4 = r.addJoint(link=[v2, 0], jType="elbow", orient=1, limits=(-180, 180))
        j5 = r.addJoint(link=[v2, 1], jType="elbow", orient=1, limits=(-180, 180))

        j6 = r.addJoint(link=[j2, 0], jType="wrist", orient=0, limits=(-180, 180))
        j7 = r.addJoint(link=[j3, 1], jType="wrist", orient=0, limits=(-180, 180))

        j8 = r.addJoint(link=[j4, 0], jType="wrist", orient=0, limits=(-180, 180))
        j9 = r.addJoint(link=[j5, 1], jType="wrist", orient=0, limits=(-180, 180))

        v4 = r.addVoxel(link=[j6, 0], vType="rigid")
        v5 = r.addVoxel(link=[j7, 1], vType="rigid")
        v6 = r.addVoxel(link=[j8, 0], vType="rigid")
        v7 = r.addVoxel(link=[j9, 1], vType="rigid")

        return r

    def createWalker(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")
        v1 = r.addVoxel(link=[v, 4], vType="rigid")
        v2 = r.addVoxel(link=[v, 5], vType="rigid")

        c = r.addTendon(link=[v1, 0], length=2, orient=[0, 0], stiffness=10.0)
        v3 = r.addVoxel(link=[c, 0], vType="rigid")
        c1 = r.addTendon(link=[v3, 0], length=2, orient=[0, 1], stiffness=1.0)

        c2 = r.addTendon(link=[v2, 0], length=2, orient=[0, 1], stiffness=10.0)
        v4 = r.addVoxel(link=[c2, 0], vType="rigid")
        c3 = r.addTendon(link=[v4, 0], length=2, orient=[0, 0], stiffness=1.0)

        e1 = r.addEnd(link=[c1, 0])
        e2 = r.addEnd(link=[c3, 0])

        # todo add this part of robot class
        r.prim.AddRotateXYZOp().Set(Gf.Vec3d(0.0, 90.0, 0.0))
        r.prim.AddTranslateOp().Set(Gf.Vec3d(-10.0, 0.0, 0.0))

        r.fix(v)
        return r

    def createGripper(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")
        v1 = r.addVoxel(link=[v, 2], vType="rigid")
        v2 = r.addVoxel(link=[v, 3], vType="rigid")

        c = r.addTendon(link=[v1, 0], length=2, orient=[0, 0], stiffness=10.0)
        v3 = r.addVoxel(link=[c, 0], vType="rigid")
        c1 = r.addTendon(link=[v3, 0], length=2, orient=[0, 1], stiffness=1.0)

        c2 = r.addTendon(link=[v2, 0], length=2, orient=[0, 1], stiffness=10.0)
        v4 = r.addVoxel(link=[c2, 0], vType="rigid")
        c3 = r.addTendon(link=[v4, 0], length=2, orient=[0, 0], stiffness=1.0)

        # todo add this part of robot class
        r.prim.AddRotateXYZOp().Set(Gf.Vec3d(0.0, 90.0, 0.0))
        r.prim.AddTranslateOp().Set(Gf.Vec3d(-10.0, 0.0, 0.0))

        r.fix(v)
        return r

    def createSnake(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        c = r.addTendon(link=[v, 0], length=3, orient=[0, 0], stiffness=2.0)
        v3 = r.addVoxel(link=[c, 0], vType="rigid")
        c1 = r.addTendon(link=[v3, 0], length=3, orient=[0, 1], stiffness=2.0)
        v4 = r.addVoxel(link=[c1, 0], vType="rigid")
        c2 = r.addTendon(link=[v4, 0], length=3, orient=[0, 0], stiffness=2.0)
        v5 = r.addVoxel(link=[c2, 0], vType="rigid")

        r.fix(v)
        return r

    def createDog(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")
        v1 = r.addVoxel(link=[v, 4], vType="rigid")
        v2 = r.addVoxel(link=[v, 5], vType="rigid")

        c = r.addTendon(link=[v1, 0], length=1, orient=[0, 0], stiffness=10.0)
        v3 = r.addVoxel(link=[c, 0], vType="rigid")
        c1 = r.addTendon(link=[v3, 0], length=1, orient=[0, 1], stiffness=1.0)

        c2 = r.addTendon(link=[v2, 0], length=1, orient=[0, 1], stiffness=10.0)
        v4 = r.addVoxel(link=[c2, 0], vType="rigid")
        c3 = r.addTendon(link=[v4, 0], length=1, orient=[0, 0], stiffness=1.0)

        e1 = r.addEnd(link=[c1, 0])
        e2 = r.addEnd(link=[c3, 0])

        v5 = r.addVoxel(link=[v, 3], vType="rigid")
        v6 = r.addVoxel(link=[v1, 3], vType="rigid")
        v7 = r.addVoxel(link=[v2, 3], vType="rigid")

        v5 = r.addVoxel(link=[v5, 3], vType="rigid")
        v6 = r.addVoxel(link=[v6, 3], vType="rigid")
        v7 = r.addVoxel(link=[v7, 3], vType="rigid")

        v5 = r.addVoxel(link=[v5, 3], vType="rigid")
        v6 = r.addVoxel(link=[v6, 3], vType="rigid")
        v7 = r.addVoxel(link=[v7, 3], vType="rigid")

        v5 = r.addVoxel(link=[v5, 3], vType="rigid")
        v6 = r.addVoxel(link=[v6, 3], vType="rigid")
        v7 = r.addVoxel(link=[v7, 3], vType="rigid")

        c = r.addTendon(link=[v6, 0], length=1, orient=[0, 0], stiffness=10.0)
        v3 = r.addVoxel(link=[c, 0], vType="rigid")
        c1 = r.addTendon(link=[v3, 0], length=1, orient=[0, 1], stiffness=1.0)

        c2 = r.addTendon(link=[v7, 0], length=1, orient=[0, 1], stiffness=10.0)
        v4 = r.addVoxel(link=[c2, 0], vType="rigid")
        c3 = r.addTendon(link=[v4, 0], length=1, orient=[0, 0], stiffness=1.0)

        # todo add this part of robot class
        r.prim.AddRotateXYZOp().Set(Gf.Vec3d(0.0, 90.0, 0.0))
        r.prim.AddTranslateOp().Set(Gf.Vec3d(-10.0, 0.0, 0.0))

        r.fix(v)
        return r

    def createBillE(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        j1 = r.addJoint(link=[v, 4], jType="elbow", orient=0, limits=(-180, 180))

        v1 = r.addVoxel(link=[j1, 4], vType="rigid")
        v2 = r.addVoxel(link=[v1, 4], vType="rigid")

        j2 = r.addJoint(link=[v2, 0], jType="wrist", orient=0, limits=(-180, 180))

        v3 = r.addVoxel(link=[j2, 0], vType="rigid")
        v4 = r.addVoxel(link=[v3, 4], vType="rigid")

        j3 = r.addJoint(link=[v4, 4], jType="elbow", orient=0, limits=(-180, 180))

        v5 = r.addVoxel(link=[j3, 4], vType="rigid")

        r.fix(v)
        return r

    def createBillE2(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        vv = r.addVoxel(link=[None, 0], vType="control")
        v1 = r.addVoxel(link=[vv, 4], vType="rigid")
        j1 = r.addJoint(link=[v1, 4], jType="wrist", orient=0, limits=(-180, 180))

        v1 = r.addVoxel(link=[j1, 4], vType="rigid")

        j2 = r.addJoint(link=[v1, 0], jType="elbow", orient=0, limits=(-180, 180))
        v2 = r.addVoxel(link=[j2, 0], vType="rigid")
        v3 = r.addVoxel(link=[v2, 4], vType="rigid")

        j3 = r.addJoint(link=[v3, 2], jType="wrist", orient=0, limits=(-180, 180))

        v2 = r.addVoxel(link=[j3, 2], vType="rigid")
        v3 = r.addVoxel(link=[v2, 5], vType="rigid")

        j2 = r.addJoint(link=[v3, 0], jType="elbow", orient=0, limits=(-180, 180))

        v1 = r.addVoxel(link=[j2, 0], vType="rigid")
        j1 = r.addJoint(link=[v1, 5], jType="wrist", orient=0, limits=(-180, 180))

        v = r.addVoxel(link=[j1, 5], vType="rigid")
        v = r.addVoxel(link=[v, 5], vType="rigid")

        e1 = r.addEnd(link=[v, 5])
        e2 = r.addEnd(link=[vv, 5])

        return r

    def createLongRover(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        v1 = r.addVoxel(link=[v, 2], vType="rigid")

        j1 = r.addJoint(link=[v1, 2], jType="elbow", orient=0, limits=(-180, 180))

        v2 = r.addVoxel(link=[j1, 2], vType="rigid")
        v3 = r.addJoint(link=[v2, 2], jType="elbow", orient=0, limits=(-180, 180))

        j2 = r.addJoint(link=[v1, 0], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v1, 1], jType="elbow", orient=1, limits=(-180, 180))
        j4 = r.addJoint(link=[v2, 0], jType="elbow", orient=1, limits=(-180, 180))
        j5 = r.addJoint(link=[v2, 1], jType="elbow", orient=1, limits=(-180, 180))

        j6 = r.addJoint(link=[j2, 0], jType="wrist", orient=0, limits=(-180, 180))
        j7 = r.addJoint(link=[j3, 1], jType="wrist", orient=0, limits=(-180, 180))
        j8 = r.addJoint(link=[j4, 0], jType="wrist", orient=0, limits=(-180, 180))
        j9 = r.addJoint(link=[j5, 1], jType="wrist", orient=0, limits=(-180, 180))

        v4 = r.addVoxel(link=[j6, 0], vType="rigid")
        v5 = r.addVoxel(link=[j7, 1], vType="rigid")
        v6 = r.addVoxel(link=[j8, 0], vType="rigid")
        v7 = r.addVoxel(link=[j9, 1], vType="rigid")

        ##################################

        v1 = r.addVoxel(link=[v3, 2], vType="rigid")

        j1 = r.addJoint(link=[v1, 2], jType="elbow", orient=0, limits=(-180, 180))

        v2 = r.addVoxel(link=[j1, 2], vType="rigid")

        j2 = r.addJoint(link=[v1, 0], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v1, 1], jType="elbow", orient=1, limits=(-180, 180))
        j4 = r.addJoint(link=[v2, 0], jType="elbow", orient=1, limits=(-180, 180))
        j5 = r.addJoint(link=[v2, 1], jType="elbow", orient=1, limits=(-180, 180))

        j6 = r.addJoint(link=[j2, 0], jType="wrist", orient=0, limits=(-180, 180))
        j7 = r.addJoint(link=[j3, 1], jType="wrist", orient=0, limits=(-180, 180))
        j8 = r.addJoint(link=[j4, 0], jType="wrist", orient=0, limits=(-180, 180))
        j9 = r.addJoint(link=[j5, 1], jType="wrist", orient=0, limits=(-180, 180))

        v4 = r.addVoxel(link=[j6, 0], vType="rigid")
        v5 = r.addVoxel(link=[j7, 1], vType="rigid")
        v6 = r.addVoxel(link=[j8, 0], vType="rigid")
        v7 = r.addVoxel(link=[j9, 1], vType="rigid")

        return r

    def create2Handed(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        v2 = r.addVoxel(link=[v, 2], vType="rigid")
        v3 = r.addVoxel(link=[v2, 2], vType="rigid")

        v4 = r.addVoxel(link=[v3, 2], vType="rigid")
        v5 = r.addVoxel(link=[v4, 2], vType="rigid")

        j2 = r.addJoint(link=[v3, 0], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v3, 1], jType="elbow", orient=1, limits=(-180, 180))
        v2 = r.addVoxel(link=[j2, 0], vType="rigid")
        v3 = r.addVoxel(link=[j3, 1], vType="rigid")
        j2 = r.addJoint(link=[v2, 0], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v3, 1], jType="elbow", orient=1, limits=(-180, 180))
        v2 = r.addVoxel(link=[j2, 0], vType="rigid")
        v3 = r.addVoxel(link=[j3, 1], vType="rigid")

        e1 = r.addEnd(link=[v2, 0])
        e2 = r.addEnd(link=[v3, 1])
        return r

    def createCompliantRover(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        v1 = r.addVoxel(link=[v, 0], vType="rigid")

        c = r.addTendon(link=[v1, 0], length=1, orient=[0, 0], stiffness=2.0)

        v2 = r.addVoxel(link=[c, 0], vType="rigid")
        v3 = r.addVoxel(link=[v2, 0], vType="rigid")

        j2 = r.addJoint(link=[v1, 2], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v1, 3], jType="elbow", orient=1, limits=(-180, 180))
        j4 = r.addJoint(link=[v2, 2], jType="elbow", orient=1, limits=(-180, 180))
        j5 = r.addJoint(link=[v2, 3], jType="elbow", orient=1, limits=(-180, 180))

        j6 = r.addJoint(link=[j2, 2], jType="wrist", orient=0, limits=(-180, 180))
        j7 = r.addJoint(link=[j3, 3], jType="wrist", orient=0, limits=(-180, 180))
        j8 = r.addJoint(link=[j4, 2], jType="wrist", orient=0, limits=(-180, 180))
        j9 = r.addJoint(link=[j5, 3], jType="wrist", orient=0, limits=(-180, 180))

        v4 = r.addVoxel(link=[j6, 2], vType="rigid")
        v5 = r.addVoxel(link=[j7, 3], vType="rigid")
        v6 = r.addVoxel(link=[j8, 2], vType="rigid")
        v7 = r.addVoxel(link=[j9, 3], vType="rigid")

        r.fix(v)

        return r

    def createLoop(self, name, pos):
        # doesn't work
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        c = r.addTendon(link=[v, 0], length=2, orient=[0, 0], stiffness=2.0)
        v1 = r.addVoxel(link=[c, 0], vType="rigid")

        v2 = r.addVoxel(link=[v, 5], vType="rigid")
        v3 = r.addVoxel(link=[v1, 5], vType="rigid")

        j2 = r.addJoint(link=[v2, 5], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v3, 5], jType="elbow", orient=1, limits=(-180, 180))

        v2 = r.addVoxel(link=[j2, 5], vType="rigid")

        v2 = r.addVoxel(link=[v2, 5], vType="rigid")

        c1 = r.addTendon(link=[v2, 0], length=2, orient=[0, 0], stiffness=2.0)

        v4 = r.addVoxel(link=[c1, 0], vType="rigid")

        v3 = r.addVoxel(link=[j3, 5], vType="rigid")

    def createTL(self, name, pos):
        # doesn't work
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        c = r.addTendon(link=[v, 0], length=1, orient=[0, 0], stiffness=2.0)
        v1 = r.addVoxel(link=[c, 0], vType="rigid")

        v2 = r.addVoxel(link=[v, 5], vType="rigid")
        v3 = r.addVoxel(link=[v1, 5], vType="rigid")

        j2 = r.addJoint(link=[v2, 5], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v3, 5], jType="elbow", orient=1, limits=(-180, 180))

        v10 = r.addVoxel(link=[j2, 5], vType="rigid")
        v11 = r.addVoxel(link=[j3, 5], vType="rigid")

        v4 = r.addVoxel(link=[v10, 3], vType="rigid")
        v5 = r.addVoxel(link=[v11, 3], vType="rigid")

        v6 = r.addVoxel(link=[v4, 3], vType="rigid")
        v7 = r.addVoxel(link=[v5, 3], vType="rigid")

        v8 = r.addVoxel(link=[v6, 3], vType="rigid")
        v9 = r.addVoxel(link=[v7, 3], vType="rigid")

        ## todo check why ends don't get deleted

        e1 = r.addEnd(link=[v10, 5])
        e2 = r.addEnd(link=[v11, 5])
        e1 = r.addEnd(link=[v4, 5])
        e2 = r.addEnd(link=[v5, 5])
        e1 = r.addEnd(link=[v6, 5])
        e2 = r.addEnd(link=[v7, 5])
        e1 = r.addEnd(link=[v8, 5])
        e2 = r.addEnd(link=[v9, 5])

    def createRoverCompliantLegs(self, name, pos):
        # not working
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        v1 = r.addVoxel(link=[v, 2], vType="rigid")

        j1 = r.addVoxel(link=[v1, 2], vType="rigid")

        v2 = r.addVoxel(link=[j1, 2], vType="rigid")
        v3 = r.addVoxel(link=[v2, 2], vType="rigid")

        j2 = r.addJoint(link=[v1, 0], jType="wrist", orient=0, limits=(-180, 180))
        j3 = r.addJoint(link=[v1, 1], jType="wrist", orient=0, limits=(-180, 180))
        j4 = r.addJoint(link=[v2, 0], jType="wrist", orient=0, limits=(-180, 180))
        j5 = r.addJoint(link=[v2, 1], jType="wrist", orient=0, limits=(-180, 180))

        j6 = r.addTendon(link=[j2, 0], length=1, orient=[0, 0], stiffness=2.0)
        j7 = r.addTendon(link=[j3, 1], length=1, orient=[0, 0], stiffness=2.0)
        j8 = r.addTendon(link=[j4, 0], length=1, orient=[0, 0], stiffness=2.0)
        j9 = r.addTendon(link=[j5, 1], length=1, orient=[0, 0], stiffness=2.0)

        v4 = r.addVoxel(link=[j6, 0], vType="rigid")
        v5 = r.addVoxel(link=[j7, 1], vType="rigid")
        v6 = r.addVoxel(link=[j8, 0], vType="rigid")
        v7 = r.addVoxel(link=[j9, 1], vType="rigid")

    def create2Handed2(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        jj = r.addJoint(link=[v, 3], jType="elbow", orient=1, limits=(-180, 180))

        v3 = r.addVoxel(link=[jj, 3], vType="rigid")

        jj = r.addJoint(link=[v3, 3], jType="elbow", orient=1, limits=(-180, 180))

        v4 = r.addVoxel(link=[jj, 3], vType="rigid")

        j2 = r.addJoint(link=[v, 0], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v, 1], jType="elbow", orient=1, limits=(-180, 180))
        v2 = r.addVoxel(link=[j2, 0], vType="rigid")
        v3 = r.addVoxel(link=[j3, 1], vType="rigid")
        j2 = r.addJoint(link=[v2, 0], jType="elbow", orient=1, limits=(-180, 180))
        j3 = r.addJoint(link=[v3, 1], jType="elbow", orient=1, limits=(-180, 180))
        v2 = r.addVoxel(link=[j2, 0], vType="rigid")
        v3 = r.addVoxel(link=[j3, 1], vType="rigid")

        e1 = r.addEnd(link=[v2, 0])
        e2 = r.addEnd(link=[v3, 1])
        e3 = r.addEnd(link=[v4, 3])
        return r

    def createCrane(self, name, pos):
        r = Robot(name=name, stage=self._stage, pos=pos, size=self.size, CAD=self.usdPath)

        v = r.addVoxel(link=[None, 0], vType="control")

        v1 = r.addVoxel(link=[v, 0], vType="rigid")
        v2 = r.addVoxel(link=[v1, 0], vType="rigid")
        v3 = r.addVoxel(link=[v2, 0], vType="rigid")

        v1 = r.addVoxel(link=[v, 1], vType="rigid")
        v2 = r.addVoxel(link=[v1, 1], vType="rigid")
        v3 = r.addVoxel(link=[v2, 1], vType="rigid")

        v1 = r.addVoxel(link=[v, 2], vType="rigid")
        v2 = r.addVoxel(link=[v1, 2], vType="rigid")
        v3 = r.addVoxel(link=[v2, 2], vType="rigid")

        v1 = r.addVoxel(link=[v, 3], vType="rigid")
        v2 = r.addVoxel(link=[v1, 3], vType="rigid")
        v3 = r.addVoxel(link=[v2, 3], vType="rigid")

        v4 = r.addVoxel(link=[v, 4], vType="rigid")
        j = r.addJoint(link=[v4, 4], jType="wrist", orient=0, limits=(-180, 180))

        v5 = r.addVoxel(link=[j, 4], vType="rigid")

        j = r.addJoint(link=[v5, 4], jType="elbow", orient=1, limits=(-180, 180))

        v4 = r.addVoxel(link=[j, 4], vType="rigid")
        v5 = r.addVoxel(link=[v4, 4], vType="rigid")

        j = r.addJoint(link=[v5, 0], jType="elbow", orient=1, limits=(-180, 180))

        v4 = r.addVoxel(link=[j, 0], vType="rigid")

        return r


#########################################################################

#########################################################################