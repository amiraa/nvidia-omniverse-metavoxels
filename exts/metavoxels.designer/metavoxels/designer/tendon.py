# Copyright (c) 2022 NVIDIA CORPORATION & AFFILIATES.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
import math
import os

import carb
import numpy as np
import omni.kit.commands
import omni.usd
from omni.physx.scripts import physicsUtils
from pxr import Gf, PhysxSchema, Sdf, UsdGeom, UsdLux, UsdPhysics

FILE_DIR = os.path.dirname(os.path.realpath(__file__))


class TendonExample(omni.ext.IExt):
    def on_startup(self, ext_id):
        print("[metavoxels] Started")
        asyncio.ensure_future(self.create())

    async def create(self):
        await omni.kit.app.get_app_interface().next_update_async()

        Enable_Motion = True
        stage = omni.usd.get_context().get_stage()

        print("[metavoxels] Create demo")

        # cleanup stage
        self._stage = stage
        self._default_prim_path = stage.GetDefaultPrim().GetPath()
        children = stage.GetDefaultPrim().GetChildren()
        for child in children:
            stage.RemovePrim(child.GetPath())

        self._gravity_magnitude = 9.81  # [m/s^2]
        self._offset_amplitude = 0.3  # [m]
        self._attachment_amplitude = 0.2  # [m]
        self._motion_frequency = 0.25  # [Hz]
        self._link_dimensions = Gf.Vec3f(0.45, 0.05, 0.05)  # [m]
        self._link_density = 1000.0  # [kg/m^3]

        # link colors
        self._staticColor = Gf.Vec3f(165.0 / 255.0, 21.0 / 255.0, 21.0 / 255.0)
        self._dynamicColor = Gf.Vec3f(71.0 / 255.0, 165.0 / 255.0, 1.0)

        self.num_link = 4

        self._setup_stage()
        self._setup_articulations()
        self._enable_tendon_debug_visibility()
        if Enable_Motion:
            self._time = 0.0  # motion variable tracking sim time
            self._setup_callbacks()

    def _setup_stage(self):
        # setup y-up, SI-unit stage
        UsdGeom.SetStageUpAxis(self._stage, UsdGeom.Tokens.y)
        UsdGeom.SetStageMetersPerUnit(self._stage, 1.0)
        physicsScenePath = self._default_prim_path.AppendChild("physicsScene")
        scene = UsdPhysics.Scene.Define(self._stage, physicsScenePath)
        scene.CreateGravityDirectionAttr().Set(Gf.Vec3f(0, -1, 0))
        scene.CreateGravityMagnitudeAttr().Set(self._gravity_magnitude)

        physicsUtils.add_ground_plane(
            self._stage,
            (self._default_prim_path.AppendChild("ground")).pathString,
            "Y",
            10.00,
            Gf.Vec3f(0),
            Gf.Vec3f(0.2, 0.25, 0.25),
        )

        # setup light:
        sphereLight = UsdLux.SphereLight.Define(self._stage, self._default_prim_path.AppendChild("SphereLight"))
        sphereLight.CreateEnableColorTemperatureAttr().Set(True)
        sphereLight.CreateColorTemperatureAttr().Set(5500)
        sphereLight.CreateRadiusAttr().Set(1)
        sphereLight.CreateIntensityAttr().Set(30000)
        sphereLight.AddTranslateOp().Set(Gf.Vec3f(2.25, 3.36, 4.49))

        # set cam:
        customLayerData = {
            "cameraSettings": {
                "Perspective": {"position": Gf.Vec3d(0.0, 2.36, 2.49), "target": Gf.Vec3d(0.0, 0.98, -3.0)},
                "boundCamera": "/OmniverseKit_Persp",
            }
        }
        self._stage.GetRootLayer().customLayerData = customLayerData

    def _setup_articulations(self):
        apis = self._create_articulation("Offset_Actuation", Gf.Vec3f(0.0, 1.5, 0.5))
        self._offset_attachment = apis[0]

    def _create_moving_link(
        self, link_path: Sdf.Path, parent_path: Sdf.Path, translation: Gf.Vec3f, parent_joint_offset: Gf.Vec3f
    ):
        left_link = UsdGeom.Cube.Define(self._stage, link_path)
        left_link.GetSizeAttr().Set(1.0)
        left_link.AddTranslateOp().Set(translation)
        left_link.AddScaleOp().Set(self._link_dimensions)
        left_link.CreateDisplayColorAttr().Set([self._dynamicColor])
        omni.kit.commands.execute(
            "SetRigidBodyCommand", path=link_path, approximationShape="convexHull", kinematic=False
        )
        mass_api = UsdPhysics.MassAPI.Apply(left_link.GetPrim())
        mass_api.CreateDensityAttr().Set(self._link_density)
        joint = UsdPhysics.RevoluteJoint.Define(self._stage, link_path.AppendChild("revoluteJoint"))
        joint.CreateBody0Rel().SetTargets([parent_path])
        joint.CreateBody1Rel().SetTargets([link_path])
        joint.CreateAxisAttr("Z")
        joint.CreateLocalPos0Attr().Set(parent_joint_offset)
        joint.CreateLocalRot0Attr().Set(Gf.Quatf(0.0))
        joint.CreateLocalPos1Attr().Set(-parent_joint_offset)
        joint.CreateLocalRot1Attr().Set(Gf.Quatf(0.0))

    def _create_articulation(self, articulation_name: str, offset: Gf.Vec3f):
        basePath = self._default_prim_path.AppendChild(articulation_name + "_BaseLink")
        base = UsdGeom.Cube.Define(self._stage, basePath)
        base.GetSizeAttr().Set(1.0)
        base.AddTranslateOp().Set(offset)
        base.AddScaleOp().Set(self._link_dimensions)
        base.CreateDisplayColorAttr().Set([self._staticColor])
        omni.kit.commands.execute(
            "SetRigidBodyCommand", path=base.GetPath(), approximationShape="convexHull", kinematic=False
        )

        fixedJoint = UsdPhysics.FixedJoint.Define(
            self._stage, self._default_prim_path.AppendChild(articulation_name + "_FixedRootJoint")
        )
        fixedJoint.CreateBody1Rel().SetTargets([base.GetPath()])
        UsdPhysics.ArticulationRootAPI.Apply(fixedJoint.GetPrim())

        usdPath = FILE_DIR + "../../../data/USD/tendonAbstract/voxel_sides_1.usd"
        usdPath_b = FILE_DIR + "../../../data/USD/tendonAbstract/voxel_sides_b.usd"
        usdPath_c = FILE_DIR + "../../../data/USD/tendonAbstract/compliant_face_half.usd"

        rot = Gf.Vec3f(0, 90.0, -90)
        rot_c = Gf.Vec3f(0, 90.0, 0)
        rot_ct = Gf.Vec3f(0, -90.0, 0)

        # todo make this parametric according to link size
        scale = Gf.Vec3f(0.15, 0.15, 0.15)
        scalet = Gf.Vec3f(0.15, -0.15, 0.15)

        self.list_compliant_paths_t = []
        voxPath = self._create_voxel(
            usdPath=usdPath_b, name="voxel_base", parentPath=basePath, pos=offset, rot=rot, scale=scale
        )
        compliantPath = self._create_voxel(
            usdPath=usdPath_c, name="compliant_base_b", parentPath=basePath, pos=offset, rot=rot_c, scale=scale
        )

        compliantPath = self._create_voxel(
            usdPath=usdPath_c, name="compliant_base_t", parentPath=basePath, pos=offset, rot=rot_ct, scale=scalet
        )
        self.list_compliant_paths_t.append(compliantPath)
        joint_offset = Gf.Vec3f(0.5, 0, 0)

        for i in range(self.num_link):
            # position is 0.5 because link shape is achieved via scaling that needs to be considered in the joint pos
            link_offset = Gf.Vec3f(self._link_dimensions[0] * (i + 1), 0, 0)
            right_link_path = self._default_prim_path.AppendChild(articulation_name + "_link_" + str(i))
            self._create_moving_link(
                link_path=right_link_path,
                parent_path=basePath,
                translation=offset + link_offset,
                parent_joint_offset=joint_offset,
            )
            self._setup_spatial_tendon(num=i, base_link_path=basePath, right_link_path=right_link_path)
            basePath = right_link_path

            voxPath = self._create_voxel(
                usdPath=usdPath,
                name="voxel_link_" + str(i),
                parentPath=right_link_path,
                pos=offset + link_offset,
                rot=rot,
                scale=scale,
            )
            compliantPath = self._create_voxel(
                usdPath=usdPath_c,
                name="compliant_link_b_" + str(i) + "_",
                parentPath=right_link_path,
                pos=offset + link_offset,
                rot=rot_c,
                scale=scale,
            )
            compliantPath = self._create_voxel(
                usdPath=usdPath_c,
                name="compliant_link_t_" + str(i) + "_",
                parentPath=right_link_path,
                pos=offset + link_offset,
                rot=rot_ct,
                scale=scalet,
            )
            self.list_compliant_paths_t.append(compliantPath)

        return [0, 0]

    def _create_voxel(
        self, usdPath: str, name: str, parentPath: Sdf.Path, pos: Gf.Vec3f, rot: Gf.Vec3f, scale: Gf.Vec3f
    ):
        # todo change absolute path
        voxelPath = self._default_prim_path.AppendChild(name)

        omni.kit.commands.execute(
            "CreateReferenceCommand",
            usd_context=omni.usd.get_context(),
            path_to=voxelPath,
            asset_path=usdPath,
            instanceable=True,
        )

        omni.kit.commands.execute(
            "TransformPrim",
            path=Sdf.Path(voxelPath),
            new_transform_matrix=Gf.Matrix4d(
                0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0
            ),
        )

        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:rotateXYZ"),
            value=rot,
            prev=Gf.Vec3d(0.0, 0.0, 0.0),
        )

        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:scale"),
            value=scale,
            prev=Gf.Vec3f(1.0, 1.0, 1.0),
        )

        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path("/World/" + name + ".xformOp:translate"),
            value=pos,
            prev=Gf.Vec3d(0.0, 0.0, 0.0),
        )

        omni.kit.commands.execute(
            "MovePrim", path_from=self._default_prim_path.AppendChild(name), path_to=parentPath.AppendChild(name)
        )

        return voxelPath

    def _setup_spatial_tendon(self, num: int, base_link_path: Sdf.Path, right_link_path: Sdf.Path):
        """
        Sets up a spatial tendon with a root attachment on the left link, a regular attachment on the base, and a
        leaf attachment on the right link.

        The root and leaf attachment are at the center of gravity of the links, and the base link's attachment is
        a link half-length above the fixed-base's COG in the stage-up direction (+Y).

        The code sizes the tendon's stiffness based on the tendon geometry and link mass
        """
        base_prim = self._stage.GetPrimAtPath(base_link_path)
        right_link_prim = self._stage.GetPrimAtPath(right_link_path)

        half_length = self._link_dimensions[0] * 0.5

        # Tendon stiffness dimensioning
        # We could set the rest length to -1 to autocompute it but we calculate it here to size the tendon stiffness
        # based on a desired deviation from the rest length
        # the rest length is 2 * sqrt((2 * half_length)^2 + half_length^2) = 2 * half_length * sqrt(5)
        rest_length = 2.0 * half_length * math.sqrt(5.0)
        rest_length = self._link_dimensions[0]

        # The goal is to have the revolute joints deviate just a few degrees from the horizontal
        # and we compute the length of the tendon at that angle:
        deviationAngle = 3.0 * math.pi / 180.0
        # Distances from the base-link attachment to the root and leaf attachments
        vertical_distance = half_length * (1.0 + math.sin(deviationAngle))
        horizontal_distance = half_length * (1.0 + math.cos(deviationAngle))
        deviated_length = 2.0 * math.sqrt(vertical_distance**2.0 + horizontal_distance**2.0)

        # At rest, the force on the leaf attachment is (deviated_length - rest_length) * tendon_stiffness.
        # The force points from the moving links' COG to the fixed-base attachment.
        # The force needs to be equal to the gravity force acting on the link, projected onto the tendon.
        # An equal and opposing (in the direction of the tendon) force acts on the root-attachment link and will hold
        # that link up. In order to calculate the tendon stiffness that produces that force, we consider the forces
        # and attachment geometry at zero joint angles.
        link_mass = self._link_dimensions[0] * self._link_dimensions[1] * self._link_dimensions[2] * self._link_density
        gravity_force = self._gravity_magnitude * link_mass
        # Project onto tendon at rest length / with joints at zero angle
        tendon_force = gravity_force * 0.5 * rest_length / half_length
        # and compute stiffness to get tendon force at deviated length to hold the link:
        tendon_stiffness = tendon_force / (deviated_length - rest_length)

        tendon_stiffness = tendon_force / 0.001
        tendon_damping = 0.3 * tendon_stiffness

        attachment_local_pos = half_length / self._link_dimensions[1]

        rootApi = PhysxSchema.PhysxTendonAttachmentRootAPI.Apply(base_prim, "Root_t_" + str(num))
        # center attachment above dynamic link
        rootApi.CreateLocalPosAttr().Set(Gf.Vec3f(0.5, attachment_local_pos, 0.0))
        rootApi.CreateStiffnessAttr().Set(tendon_stiffness)
        rootApi.CreateDampingAttr().Set(tendon_damping)

        attachmentApi = PhysxSchema.PhysxTendonAttachmentAPI.Apply(right_link_prim, "Attachment_t_" + str(num))
        attachmentApi.CreateParentLinkRel().AddTarget(base_link_path)
        attachmentApi.CreateParentAttachmentAttr().Set("Root_t_" + str(num))
        # the attachment local pos takes the link scaling into account, just like the joint positions
        attachmentApi.CreateLocalPosAttr().Set(Gf.Vec3f(0.0, attachment_local_pos, 0.0))

        leafApi = PhysxSchema.PhysxTendonAttachmentLeafAPI.Apply(right_link_prim, "Leaf_t_" + str(num))
        leafApi.CreateParentLinkRel().AddTarget(right_link_path)
        leafApi.CreateParentAttachmentAttr().Set("Attachment_t_" + str(num))
        leafApi.CreateLocalPosAttr().Set(Gf.Vec3f(0.5, attachment_local_pos, 0.0))
        leafApi.CreateRestLengthAttr().Set(rest_length / 3.0)

        # bottom
        rootApi_1 = PhysxSchema.PhysxTendonAttachmentRootAPI.Apply(base_prim, "Root_b_" + str(num))
        # center attachment above dynamic link
        rootApi_1.CreateLocalPosAttr().Set(Gf.Vec3f(0.5, -attachment_local_pos, 0.0))
        rootApi_1.CreateStiffnessAttr().Set(tendon_stiffness)
        rootApi_1.CreateDampingAttr().Set(tendon_damping)

        attachmentApi_1 = PhysxSchema.PhysxTendonAttachmentAPI.Apply(right_link_prim, "Attachment_b_" + str(num))
        attachmentApi_1.CreateParentLinkRel().AddTarget(base_link_path)
        attachmentApi_1.CreateParentAttachmentAttr().Set("Root_b_" + str(num))
        # the attachment local pos takes the link scaling into account, just like the joint positions
        attachmentApi_1.CreateLocalPosAttr().Set(Gf.Vec3f(0.0, -attachment_local_pos, 0.0))

        leafApi_1 = PhysxSchema.PhysxTendonAttachmentLeafAPI.Apply(right_link_prim, "Leaf_b_" + str(num))
        leafApi_1.CreateParentLinkRel().AddTarget(right_link_path)
        leafApi_1.CreateParentAttachmentAttr().Set("Attachment_b_" + str(num))
        leafApi_1.CreateLocalPosAttr().Set(Gf.Vec3f(0.5, -attachment_local_pos, 0.0))
        leafApi_1.CreateRestLengthAttr().Set(rest_length)

        return (rootApi, attachmentApi, leafApi)

    def _enable_tendon_debug_visibility(self):
        isregistry = carb.settings.acquire_settings_interface()
        # setting to two forces debug viz of all tendons
        self._tendon_viz_mode = isregistry.get_as_int(omni.physx.bindings._physx.SETTING_DISPLAY_TENDONS)
        isregistry.set_int(omni.physx.bindings._physx.SETTING_DISPLAY_TENDONS, 2)

    def on_shutdown(self):
        isregistry = carb.settings.acquire_settings_interface()
        isregistry.set_int(omni.physx.bindings._physx.SETTING_DISPLAY_TENDONS, self._tendon_viz_mode)
        self._deregister_callbacks()

    def _setup_callbacks(self):
        # callbacks
        self._timeline = omni.timeline.get_timeline_interface()
        stream = self._timeline.get_timeline_event_stream()
        self._timeline_subscription = stream.create_subscription_to_pop(self._on_timeline_event)
        # subscribe to Physics updates:
        self._physics_update_subscription = omni.physx.get_physx_interface().subscribe_physics_step_events(
            self.on_physics_step
        )

    def _deregister_callbacks(self):
        self._timeline_subscription = None
        self._physics_update_subscription = None

    def _on_timeline_event(self, e):
        if e.type == int(omni.timeline.TimelineEventType.STOP):
            self._time = 0
            # scale all purple ones back
            for i in range(self.num_link):
                compliant_name = "link_" + str(i) + "/compliant_link_t_" + str(i) + "_/compliant_face_halved/" + "_1"
                omni.kit.commands.execute(
                    "ChangeProperty",
                    prop_path=Sdf.Path("/World/Offset_Actuation_" + compliant_name + ".xformOp:scale"),
                    value=Gf.Vec3d(1.0, 1.0, 1.0),
                    prev=Gf.Vec3f(1.0, 1.0, 1.0),
                )

                compliant_name = "link_" + str(i) + "/compliant_link_b_" + str(i) + "_/compliant_face_halved/" + "_0"
                omni.kit.commands.execute(
                    "ChangeProperty",
                    prop_path=Sdf.Path("/World/Offset_Actuation_" + compliant_name + ".xformOp:scale"),
                    value=Gf.Vec3d(1.0, 1.0, 1.0),
                    prev=Gf.Vec3f(1.0, 1.0, 1.0),
                )
            # go through purple tendons make them scale 1
            # cannot call update tendons here to properly reset the demo, because on new stage load while running,
            # Kit crashes due to a flatcache issue otherwise.

    def on_physics_step(self, dt):
        self._time += dt
        self._update_tendons()
        self._update_compliant_faces()

    def _update_compliant_faces(self):
        i = 0
        link1_name = "BaseLink" + "/Root_t_" + str(i)
        link2_name = "link_" + str(i) + "/Attachment_t_" + str(i)
        compliant_name = "link_" + str(i) + "/compliant_link_t_" + str(i) + "_/compliant_face_halved/" + "_1"
        self._update_compliance_link(link1_name, link2_name, compliant_name)

        link1_name = "BaseLink" + "/Root_b_" + str(i)
        link2_name = "link_" + str(i) + "/Attachment_b_" + str(i)
        compliant_name = "link_" + str(i) + "/compliant_link_b_" + str(i) + "_/compliant_face_halved/" + "_0"
        self._update_compliance_link(link1_name, link2_name, compliant_name)

        for i in range(self.num_link - 1):
            link1_name = "link_" + str(i) + "/Root_t_" + str(i + 1)
            link2_name = "link_" + str(i + 1) + "/Attachment_t_" + str(i + 1)
            compliant_name = (
                "link_" + str(i + 1) + "/compliant_link_t_" + str(i + 1) + "_/compliant_face_halved/" + "_1"
            )
            self._update_compliance_link(link1_name, link2_name, compliant_name)

            link1_name = "link_" + str(i) + "/Root_b_" + str(i + 1)
            link2_name = "link_" + str(i + 1) + "/Attachment_b_" + str(i + 1)
            compliant_name = (
                "link_" + str(i + 1) + "/compliant_link_b_" + str(i + 1) + "_/compliant_face_halved/" + "_0"
            )
            self._update_compliance_link(link1_name, link2_name, compliant_name)

    def _update_compliance_link(self, link1_name, link2_name, compliant_name):
        point = self._stage.GetPrimAtPath("/PhysxTendonSpatialAttachmentGeoms/_World_Offset_Actuation_" + link1_name)
        point1 = self.getWorldPosition(point)
        point = self._stage.GetPrimAtPath("/PhysxTendonSpatialAttachmentGeoms/_World_Offset_Actuation_" + link2_name)
        point2 = self.getWorldPosition(point)
        diff = point1 - point2
        dis = np.linalg.norm(np.array([diff[0], diff[1], diff[2]]))

        newScale = dis / (self._link_dimensions[0] / 2)

        primName = "/World/Offset_Actuation_" + compliant_name
        s = Gf.Vec3d(1.0, 1.0, 1.0)

        omni.kit.commands.execute(
            "ChangeProperty",
            prop_path=Sdf.Path(primName + ".xformOp:scale"),
            value=Gf.Vec3d(1.0, 1.0, newScale),
            prev=s,
        )

    def getWorldPosition(self, prim):
        # instantiate an xform cache
        xform_cache = UsdGeom.XformCache()

        # retrieve and save local and world transforms into matrix4d's
        mtrx_world = xform_cache.GetLocalToWorldTransform(prim)
        mtrx_local, resets_xform_stack = xform_cache.GetLocalTransformation(prim)

        # extract translations
        translation_world = mtrx_world.ExtractTranslation()
        translation_local = mtrx_local.ExtractTranslation()

        return translation_world

    def _update_tendons(self):
        offsetPos = self._offset_amplitude * (
            1.0 + math.sin(self._time * 2.0 * math.pi * self._motion_frequency - math.pi * 0.5)
        )
